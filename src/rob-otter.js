window.requestAnimFrame = (function(){					// von (https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)
  return  window.requestAnimationFrame       ||			// loop für canvas erneuerung
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();
(function animloop(){
  requestAnimFrame(animloop);
  render();
})();
//**************************************************
var canvas = document.createElement("canvas");      // erzeugt canvas zur laufzeit
var ctx = canvas.getContext("2d");
canvas.id = "game";
canvas.width =1500;
canvas.height = 975;
document.getElementById("canvascontainer").appendChild(canvas);
ctx.fillStyle="#013668";  // blaue dialogbox
ctx.fillRect(0, 844, 1500, 130);
ctx.fillStyle="white";               // schreibt gesichts teile
ctx.font="17px Arial";
ctx.fillText("linkes Auge",35,865);
ctx.fillText("linkes Ohr",35,885);
ctx.fillText("Nase",35,905);
ctx.fillText("Lippen",35,925);
ctx.fillText("Zaehne",35,945);
ctx.fillText("rechtes Auge",170,865);
ctx.fillText("rechtes Ohr",170,885);
ctx.fillStyle="green"			// zeichnet gesichts checkboxen
ctx.fillRect(15,848,17,17);
ctx.fillStyle="red"
ctx.fillRect(15,868,17,17);
ctx.fillRect(15,888,17,17);
ctx.fillRect(15,908,17,17);
ctx.fillRect(15,928,17,17);
ctx.fillRect(150,848,17,17);
ctx.fillRect(150,868,17,17);
ctx.fillStyle="#0B0B3B"			// zeichnet box trenner
ctx.fillRect(300,844,5,130);

//*************************************************************************************************
// 									  VARIABLEN DEKLARATION		  								  *
//*************************************************************************************************
var zimmer="terasse"   // enthält den zuladenden background
var earlfound=false;
var lipsfound=false;
var nosefound=false;
var earrfound=false;
var teethfound=false;
var eyefound=false;
var allfound=false;



//*************************************************************************************************
// 											IMAGE DEKLARATION									  *
//*************************************************************************************************
var terasseready = false;                         													//terasse wird geladen        
var terasseimage = new Image();
terasseimage.onload = function() { 
    terasseready = true;
};
terasseimage.src = "graphic/me-terasse.png";
//*************************************************************************************************
var flur1ready = false;                                												// flur1 wird geladen 
var flur1image = new Image();
flur1image.onload = function() { 
    flur1ready = true;
};
flur1image.src = "graphic/me-flur1.png";
//*************************************************************************************************
var heroready = false;                              												// hero wird geladen
var heroimage = new Image();
heroimage.onload = function() { 
    heroready = true;
};
heroimage.src = "graphic/robo.png";
//*************************************************************************************************
var bibready = false;                              													// bib wird geladen
var bibimage = new Image();
bibimage.onload = function() { 
    bibready = true;
};
bibimage.src = "graphic/bib.png";
//*************************************************************************************************
var earlready = false;                              												// left ear wird geladen
var earlimage = new Image();
earlimage.onload = function() { 
    earlready = true;
};
earlimage.src = "graphic/earlefttrans.png";
//*************************************************************************************************
var pointerready = false;                              												// pointer wird geladen
var pointerimage = new Image();
pointerimage.onload = function() { 
    pointerready = true;
};
pointerimage.src = "graphic/Pointer.png";
//*************************************************************************************************
var caferready = false;                              												// cafe rechts wird geladen
var caferimage = new Image();
caferimage.onload = function() { 
    caferready = true;
};
caferimage.src = "graphic/cafer.png";
//*************************************************************************************************
var cafelready = false;                              												// cafe links wird geladen
var cafelimage = new Image();
cafelimage.onload = function() { 
    cafelready = true;
};
cafelimage.src = "graphic/cafel.png";
//*************************************************************************************************
var microlready = false;                              												// microwelle links wird geladen
var microlimage = new Image();
microlimage.onload = function() { 
    microlready = true;
};
microlimage.src = "graphic/micro-l.png";
//*************************************************************************************************
var microrready = false;                              												// mikrowelle rechts wird geladen
var  microrimage = new Image();
 microrimage.onload = function() { 
     microrready = true;
};
 microrimage.src = "graphic/micro-r.png";
//*************************************************************************************************
var lipsready = false;                              												// lippen wird geladen
var  lipsimage = new Image();
 lipsimage.onload = function() { 
     lipsready = true;
};
 lipsimage.src = "graphic/lips.png";
//*************************************************************************************************
var treppe1zuready = false;                              												// treppe1zu wird geladen
var treppe1zuimage = new Image();
 treppe1zuimage.onload = function() { 
     treppe1zuready = true;
};
 treppe1zuimage.src = "graphic/treppe1zu.png";
//*************************************************************************************************
var treppe1aufready = false;                              												// treppe1zu wird geladen
var treppe1aufimage = new Image();
 treppe1aufimage.onload = function() { 
     treppe1aufready = true;
};
 treppe1aufimage.src = "graphic/treppe1auf.png";
//*************************************************************************************************
var treppe1selready = false;                              												// treppe1sel wird geladen
var treppe1selimage = new Image();
 treppe1selimage.onload = function() { 
     treppe1selready = true;
};
 treppe1selimage.src = "graphic/treppe1sel.png";
//*************************************************************************************************
var naseready = false;                              												// nase wird geladen
var naseimage = new Image();
 naseimage.onload = function() { 
     naseready = true;
};
 naseimage.src = "graphic/nase.png";
//*************************************************************************************************
var treppe2aufready = false;                              												// treppe2auf wird geladen
var treppe2aufimage = new Image();
 treppe2aufimage.onload = function() { 
     treppe2aufready = true;
};
 treppe2aufimage.src = "graphic/treppe2auf.png";
//*************************************************************************************************
var treppe0aufready = false;                              												// treppe0auf wird geladen
var treppe0aufimage = new Image();
 treppe0aufimage.onload = function() { 
     treppe0aufready = true;
};
 treppe0aufimage.src = "graphic/treppe0auf.png";
//*************************************************************************************************
var treppe0selready = false;                              												// treppe0sel wird geladen
var treppe0selimage = new Image();
 treppe0selimage.onload = function() { 
     treppe0selready = true;
};
 treppe0selimage.src = "graphic/treppe0sel.png";
//*************************************************************************************************
var treppe2selready = false;                              												// treppe2sel wird geladen
var treppe2selimage = new Image();
 treppe2selimage.onload = function() { 
     treppe2selready = true;
};
 treppe2selimage.src = "graphic/treppe2sel.png";
//*************************************************************************************************
var garageready = false;                              												// garage wird geladen
var garageimage = new Image();
 garageimage.onload = function() { 
     garageready = true;
};
 garageimage.src = "graphic/garage.png";
//*************************************************************************************************
var earrready = false;                              												// right ear wird geladen
var earrimage = new Image();
earrimage.onload = function() { 
    earrready = true;
};
earrimage.src = "graphic/earrighttrans.png";
//*************************************************************************************************
var treppe0zuready = false;                              												// treppe2sel wird geladen
var treppe0zuimage = new Image();
 treppe0zuimage.onload = function() { 
     treppe0zuready = true;
};
 treppe0zuimage.src = "graphic/treppe0zu.png";
//*************************************************************************************************
var flur2ready = false;                                												// flur1 wird geladen 
var flur2image = new Image();
flur2image.onload = function() { 
    flur2ready = true;
};
flur2image.src = "graphic/me-flur2.png";
//*************************************************************************************************
var treppe2zuready = false;                              												// treppe2sel wird geladen
var treppe2zuimage = new Image();
 treppe2zuimage.onload = function() { 
     treppe2zuready = true;
};
 treppe2zuimage.src = "graphic/treppe2zu.png";
//*************************************************************************************************
var poolready = false;                                												// flur1 wird geladen 
var poolimage = new Image();
poolimage.onload = function() { 
    poolready = true;
};
poolimage.src = "graphic/pool.png";
//*************************************************************************************************
var slzready = false;                              												// treppe2sel wird geladen
var slzimage = new Image();
 slzimage.onload = function() { 
     slzready = true;
};
 slzimage.src = "graphic/slz.png";
//*************************************************************************************************
var teethready = false;                              												// treppe2sel wird geladen
var teethimage = new Image();
 teethimage.onload = function() { 
     teethready = true;
};
 teethimage.src = "graphic/teeth.png";
//*************************************************************************************************
var eyeready = false;                              												// treppe2sel wird geladen
var eyeimage = new Image();
 eyeimage.onload = function() { 
     eyeready = true;
};
 eyeimage.src = "graphic/eye.png";
//*************************************************************************************************

var treppe1zuendready = false;                              												// treppe2sel wird geladen
var treppe1zuendimage = new Image();
 treppe1zuendimage.onload = function() { 
     treppe1zuendready = true;
};
 treppe1zuendimage.src = "graphic/treppe1zuend.png";
//*************************************************************************************************
var treppe1aufendready = false;                              												// treppe2sel wird geladen
var treppe1aufendimage = new Image();
 treppe1aufendimage.onload = function() { 
    treppe1aufendready = true;
};
 treppe1aufendimage.src = "graphic/treppe1aufend.png";
//*************************************************************************************************
var treppe1selendready = false;                              												// treppe2sel wird geladen
var treppe1selendimage = new Image();
 treppe1selendimage.onload = function() { 
     treppe1selendready = true;
};
 treppe1selendimage.src = "graphic/treppe1selend.png";
//*************************************************************************************************
var endready = false;                              												// treppe2sel wird geladen
var endimage = new Image();
 endimage.onload = function() { 
     endready = true;
};
 endimage.src = "graphic/end.png";
//*************************************************************************************************



//*************************************************************************************************
//                                     KLASSEN DEKLARATIONEN                                      *
//*************************************************************************************************
var hero = {																						// erzeugt hero & pointer mit eigenschaften	
	speed: 100,
	x: 1200,
	y: 750
};	
var pointer = {
	speed: 100,
	x: -10,
	y: -10
}



//*************************************************************************************************
//                                   TASTEN DRUCK ERKENNEN                                        *
//*************************************************************************************************
var keysDown = {};									// liste von runtergedrückten keyup
addEventListener("keydown", function (e) {			// key wird hinzugefügt
	keysDown[e.keyCode] = true;
}, false);
addEventListener("keyup", function (e) {			// key wird gelöscht
	delete keysDown[e.keyCode];
	document.getElementById("move").pause()
}, false);



//*************************************************************************************************
//                                            UPDATE                                              *
//*************************************************************************************************
var update = function (modifier) {																	// update Funktion für hero & Pointer position
//*************************************************************************************************
//                                     UPDATE - TERASSE                                           *
//*************************************************************************************************
if(zimmer == "terasse"){
	if (38 in keysDown) { // key up
		document.getElementById("move").play()
		if (hero.x < 462 || hero.x > 998){ 
		    if ( hero.x > 224 && hero.x < 383 ){
				if (hero.y - hero.speed * modifier > 47 && hero.x > 337 && hero.x < 383 ){
				  hero.y -= hero.speed * modifier;
				}
				else if (hero.y - hero.speed * modifier > 87){
					hero.y -= hero.speed * modifier;
				}
		    }
			else if (hero.y - hero.speed * modifier > 87 && hero.x > 1222 && hero.x < 1273 ){
				hero.y -= hero.speed * modifier;
			}
			else if (hero.y - hero.speed * modifier > 121){
				hero.y -= hero.speed * modifier;
			}
	    }
		if (hero.x > 462 && hero.x < 998 && hero.y < 220){
			if (hero.y - hero.speed * modifier > 87 && hero.x > 691 && hero.x < 740 ){
				hero.y -= hero.speed * modifier; 
		    }
			else if (hero.y - hero.speed * modifier > 87 && hero.x > 952 && hero.x < 998 ){
				hero.y -= hero.speed * modifier;
			}
			else if (hero.y - hero.speed * modifier > 121){
				hero.y -= hero.speed * modifier;
			}
		}
		if (hero.y - hero.speed * modifier > 121 && hero.x > 462 && hero.x < 998 && hero.y < 220){ 
		   hero.y -= hero.speed * modifier; 
	    }
		if (hero.y - hero.speed * modifier > 528 && hero.x > 462 && hero.x < 998 && hero.y > 220){ 
		   hero.y -= hero.speed * modifier; 
	    }
	}
//*************************************************************************************************
	if (40 in keysDown) { // key down
		document.getElementById("move").play()
		if(hero.x < 462 || hero.x > 998){
			if(hero.y + hero.speed * modifier < 764 && hero.x < 1139 && hero.x > 319 )  {
				hero.y += hero.speed * modifier;
			}
			if(hero.y + hero.speed * modifier < 614 && (hero.x > 1139 || hero.x < 319)) { // ecke
				hero.y += hero.speed * modifier;			
			}
		} 
		if(hero.x > 462 && hero.x < 998 ){
			if(hero.y + hero.speed * modifier < 764 && hero.y > 503  ) { // ecke
				hero.y += hero.speed * modifier;			
			}	
			if(hero.y + hero.speed * modifier < 220 ){
				hero.y += hero.speed * modifier;	
			}
		}
	}
//*************************************************************************************************
	if (37 in keysDown) { // key left
	document.getElementById("move").play()
	    if (hero.y < 87 ){
			if(hero.x - hero.speed * modifier > 337){
				hero.x -= hero.speed * modifier;
			}
		}
		else{
			if (hero.y < 121){
				if (hero.x - hero.speed * modifier > 224 && hero.x < 383 ){
					hero.x -= hero.speed * modifier;
				}
				else if (hero.x - hero.speed * modifier > 1222 && hero.x < 1270){
					hero.x -= hero.speed * modifier;
				}
				else if (hero.x - hero.speed * modifier > 691 && hero.x < 740){
					hero.x -= hero.speed * modifier;
				}
				else if (hero.x - hero.speed * modifier > 952 && hero.x < 998){
					hero.x -= hero.speed * modifier;
				}
			}
			else{
				if (hero.y < 220 || hero.y > 527){ 
					if(hero.x - hero.speed * modifier > 189 && hero.y <= 614){
						hero.x -= hero.speed * modifier;
					}
					if(hero.x - hero.speed * modifier > 319 && hero.y >= 614){
						hero.x -= hero.speed * modifier;
					}
				}	
				if (hero.y > 220 && hero.y < 527){
					if (hero.x < 503 && hero.x - hero.speed * modifier > 189){
						hero.x -= hero.speed * modifier;
					}
					if (hero.x > 503 && hero.x - hero.speed * modifier > 998){
						hero.x -= hero.speed * modifier;
					}
				}
			}
		}
	}
//*************************************************************************************************
	if (39 in keysDown) { // key right
	document.getElementById("move").play()
		if (hero.y < 121){
			if (hero.x + hero.speed * modifier < 383 && hero.x > 224 ){
				hero.x += hero.speed * modifier;
			}
			else if (hero.x + hero.speed * modifier < 1270 && hero.x > 1222){
				hero.x += hero.speed * modifier;
			}
			else if (hero.x + hero.speed * modifier < 740 && hero.x > 691){
				hero.x += hero.speed * modifier;
			}
			else if (hero.x + hero.speed * modifier < 998 && hero.x > 952){
				hero.x += hero.speed * modifier;
			}	
		}
		else{
			if (hero.y < 220 || hero.y > 527){ 
				if(hero.x + hero.speed * modifier < 1271 && hero.y <= 614 ){
					hero.x += hero.speed * modifier			
				}
				if(hero.x + hero.speed * modifier < 1139 && hero.y >= 614 ){
					hero.x += hero.speed * modifier			
				}
			}		
			if (hero.y > 220 && hero.y < 527){
				if (hero.x < 462 && hero.x + hero.speed * modifier < 462){
					hero.x += hero.speed * modifier;
				}
				if (hero.x > 998 && hero.x + hero.speed * modifier < 1271 ){
					hero.x += hero.speed * modifier;
				}
			}
		}	 
	}
//*************************************************************************************************
	if (hero.y < 50){
		zimmer = "flur1" 
		hero.y = 750;
		hero.x = 180;
		document.getElementById("move").pause()
	}
}
//*************************************************************************************************
//                                     UPDATE - Flur1                                             *
//*************************************************************************************************
else if(zimmer == "flur1"){																			
	if (38 in keysDown) { 	
document.getElementById("move").play()	// key up
		if (hero.x < 54 && hero.y - hero.speed * modifier > 339){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 54 && hero.x < 431 && hero.y - hero.speed * modifier > 162){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 431 && hero.x < 732 ){
			if(hero.x > 514 && hero.x < 646 && hero.y - hero.speed * modifier > 14){
				hero.y -= hero.speed * modifier;	
			}
			else if(hero.y - hero.speed * modifier > 53){
				hero.y -= hero.speed * modifier;
			}
		}
		if (hero.x > 732 && hero.y - hero.speed * modifier > 199){
				hero.y -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
document.getElementById("move").play()	
		if(hero.x < 54 && hero.y + hero.speed * modifier < 384){ 										// bibliothek
			hero.y += hero.speed * modifier;
		}
		if (hero.x > 54 && hero.x < 312 ){ 																//1. stufe unten 
			if (hero.x > 156 && hero.x < 201 && hero.y + hero.speed * modifier < 788 ){						// terasse
				hero.y += hero.speed * modifier;
			}
			else if( hero.y + hero.speed * modifier < 748){													// rest
				hero.y += hero.speed * modifier;
			}
		}
		if(hero.x > 312 && hero.x < 681 ){ 																// 2. stufe unten
			if (hero.x > 617 && hero.x < 661 && hero.y + hero.speed * modifier < 599 ){ 					// cafete
				hero.y += hero.speed * modifier;
			}
			else if(hero.y + hero.speed * modifier < 559){ 													//rest
				hero.y += hero.speed * modifier;
			}
		}
		if (hero.x > 681 && hero.y + hero.speed * modifier  < 469){										// 3. stufe unten
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	
document.getElementById("move").play()	// key left
		if (hero.x < 617){ 																				// links von cafeteria
			if( hero.y > 163 && hero.y < 749){ 																//zwischen terassen wand und gegenüber
				if(hero.y > 339 && hero.y < 384 && hero.x - hero.speed * modifier > 14 ){ 						// bib eingang
					hero.x -= hero.speed * modifier;
				}
				else if(hero.x - hero.speed * modifier > 54 ){  												//rest
					hero.x -= hero.speed * modifier;
				}
			}
			if(hero.x - hero.speed * modifier > 432 && hero.y < 163 && hero.y > 54){ 						// wand links von treppenhaus
				hero.x -= hero.speed * modifier;
			}
			if (hero.y < 54 && hero.x - hero.speed * modifier > 514 ){ 										// treppenhaus zugang
				hero.x -= hero.speed * modifier;
			}
			if(hero.y > 749 && hero.x - hero.speed * modifier > 155){										// terassen zugang
				hero.x -= hero.speed * modifier;
			}
		}
		else{ 																							// else
			if (hero.y > 560 && hero.x - hero.speed * modifier > 617){ 										// cafete
					hero.x -= hero.speed * modifier;
			}
			else if(hero.y < 560){ 																			// rest 
				hero.x -= hero.speed * modifier;
			}
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
document.getElementById("move").play()	// key right
		if (hero.y > 560 && hero.y < 749 ){ //unterster bereich
			if (hero.x > 617 && hero.x + hero.speed * modifier < 661){ //cafete
				hero.x += hero.speed * modifier;
			}
			else if (hero.x < 617 && hero.x + hero.speed * modifier < 313){ // rest
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y < 560 && hero.y > 470 && hero.x + hero.speed * modifier < 681){ //2. unterster bereich
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 470 && hero.y > 199 && hero.x + hero.speed * modifier < 1404){  // langer flur bereich
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 199 && hero.x < 838){  // bereich zum treppenhaus
			if (hero.y < 54 && hero.x + hero.speed * modifier < 646){ // eingang treppenhaus
				hero.x += hero.speed * modifier;
			}
			if(hero.y > 54 && hero.x + hero.speed * modifier < 732){ //rest
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y > 749 &&  hero.x + hero.speed * modifier < 202){//terasse
			hero.x += hero.speed * modifier;
		}	
	}
//*************************************************************************************************
		if (hero.y < 20){
			if (allfound){
				zimmer="treppe1zuend"
			}
			else{
				zimmer="treppe1zu"
			}
		hero.x= 940
		hero.y= 748
	}
	if (hero.y > 786){ 																				// eingang terasse
		zimmer="terasse"
		hero.x= 362;
		hero.y= 86;
			document.getElementById("move").pause()
	}
	if (hero.x < 16){ 																				// eingang bibliothek
		zimmer="bib";
		hero.x=-50;
		hero.y=-50;
		pointer.x = 500;
		pointer.y = 200
			document.getElementById("move").pause()
	}
	if (hero.x > 617 && hero.x < 661 && hero.y > 595 ){ 					// cafete
		zimmer="cafer"
		hero.x=-50;
		hero.y=-50;
		pointer.x = 500;
		pointer.y = 200
			document.getElementById("move").pause()
	}

}
//*************************************************************************************************
//                                     UPDATE - Flur2                                             *
//*************************************************************************************************
else if(zimmer == "flur2"){																			
	if (38 in keysDown) { 	
document.getElementById("move").play()	// key up
		if (hero.x < 54 && hero.y - hero.speed * modifier > 339){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 54 && hero.x < 431 && hero.y - hero.speed * modifier > 162){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 431 && hero.x < 732 ){
			if(hero.x > 514 && hero.x < 646 && hero.y - hero.speed * modifier > 14){
				hero.y -= hero.speed * modifier;	
			}
			else if(hero.y - hero.speed * modifier > 53){
				hero.y -= hero.speed * modifier;
			}
		}
		if (hero.x > 732 && hero.y - hero.speed * modifier > 199){
				hero.y -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
document.getElementById("move").play()	
		if(hero.x < 54 && hero.y + hero.speed * modifier < 384){ 										// bibliothek
			hero.y += hero.speed * modifier;
		}
		if(hero.x > 54 && hero.x < 681 ){ 																// 2. stufe unten
			if (hero.x > 617 && hero.x < 661 && hero.y + hero.speed * modifier < 599 ){ 					// cafete
				hero.y += hero.speed * modifier;
			}
			else if(hero.y + hero.speed * modifier < 559){ 													//rest
				hero.y += hero.speed * modifier;
			}
		}
		if (hero.x > 681 && hero.y + hero.speed * modifier  < 469){										// 3. stufe unten
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	
document.getElementById("move").play()	// key left
		if (hero.x < 617){ 																				// links von cafeteria
			if( hero.y > 163 && hero.y < 749){ 																//zwischen terassen wand und gegenüber
				if(hero.y > 339 && hero.y < 384 && hero.x - hero.speed * modifier > 14 ){ 						// bib eingang
					hero.x -= hero.speed * modifier;
				}
				else if(hero.x - hero.speed * modifier > 54 ){  												//rest
					hero.x -= hero.speed * modifier;
				}
			}
			if(hero.x - hero.speed * modifier > 432 && hero.y < 163 && hero.y > 54){ 						// wand links von treppenhaus
				hero.x -= hero.speed * modifier;
			}
			if (hero.y < 54 && hero.x - hero.speed * modifier > 514 ){ 										// treppenhaus zugang
				hero.x -= hero.speed * modifier;
			}
			if(hero.y > 749 && hero.x - hero.speed * modifier > 155){										// terassen zugang
				hero.x -= hero.speed * modifier;
			}
		}
		else{ 																							// else
			if (hero.y > 560 && hero.x - hero.speed * modifier > 617){ 										// cafete
					hero.x -= hero.speed * modifier;
			}
			else if(hero.y < 560){ 																			// rest 
				hero.x -= hero.speed * modifier;
			}
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
document.getElementById("move").play()	// key right
		if (hero.y > 560 && hero.y < 749 ){ //unterster bereich
			if (hero.x > 617 && hero.x + hero.speed * modifier < 661){ //cafete
				hero.x += hero.speed * modifier;
			}
			else if (hero.x < 617 && hero.x + hero.speed * modifier < 313){ // rest
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y < 560 && hero.y > 470 && hero.x + hero.speed * modifier < 681){ //2. unterster bereich
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 470 && hero.y > 199 && hero.x + hero.speed * modifier < 1404){  // langer flur bereich
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 199 && hero.x < 838){  // bereich zum treppenhaus
			if (hero.y < 54 && hero.x + hero.speed * modifier < 646){ // eingang treppenhaus
				hero.x += hero.speed * modifier;
			}
			if(hero.y > 54 && hero.x + hero.speed * modifier < 732){ //rest
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y > 749 &&  hero.x + hero.speed * modifier < 202){//terasse
			hero.x += hero.speed * modifier;
		}	
	}
//*************************************************************************************************
		if (hero.y < 20){
		zimmer="treppe2zu"
		hero.x= 940
		hero.y= 748
	}
	if (hero.x < 16){ 																				// eingang bibliothek
		zimmer="pool";
		hero.x=-50;
		hero.y=-50;
		pointer.x = 500;
		pointer.y = 200
		document.getElementById("move").pause()
	}
	if (hero.x > 617 && hero.x < 661 && hero.y > 595 ){ 					// cafete
		zimmer="slz"
		hero.x=-50;
		hero.y=-50;
		pointer.x = 500;
		pointer.y = 200
			document.getElementById("move").pause()
	}
}
//*************************************************************************************************
//                               UPDATE - treppe1zu / treppe 2 zu                                 *
//*************************************************************************************************
else if(zimmer == "treppe1zu" || zimmer=="treppe2zu"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
			if(hero.y > 590 &&  hero.x > 995 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}				
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 788 ){
			hero.y += hero.speed * modifier;
		}
		if( hero.x< 862 || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 748){
				hero.y += hero.speed * modifier;
			}			
		}		
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	
		document.getElementById("move").play()	// key left
		if(hero.y < 522 && hero.x - hero.speed * modifier > 799){
			hero.x -= hero.speed * modifier;	
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 522 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if (hero.y > 522 && hero.y < 590 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x + hero.speed * modifier < 1003 ){
			hero.x += hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
	}
//*************************************************************************************************
	if (hero.y > 785 && zimmer=="treppe1zu"){
		zimmer = "flur1" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (13 in keysDown && hero.x < 810 && hero.y > 180 && hero.y < 394 && zimmer=="treppe1zu") {
		if (allfound){
				zimmer="treppe1aufend"
			}
			else{
				zimmer="treppe1auf"
			}
	}
	if (hero.y > 785 && zimmer=="treppe2zu"){
		zimmer = "flur2" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (13 in keysDown && hero.x < 810 && hero.y > 180 && hero.y < 394 && zimmer=="treppe2zu") {
		zimmer="treppe2auf"  
	}
}
//*************************************************************************************************
//                                        UPDATE - treppe1zuend                                   *
//*************************************************************************************************
else if(zimmer == "treppe1zuend"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 14 ){
			hero.y -= hero.speed * modifier;
		}	
		if(hero.y < 590 && (hero.x < 862 || hero.x > 995) && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 788 ){
			hero.y += hero.speed * modifier;
		}
		if( hero.x< 862 || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 748){
				hero.y += hero.speed * modifier;
			}			
		}		
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	
		document.getElementById("move").play()	// key left
		if(hero.y < 522 && hero.y > 54 && hero.x - hero.speed * modifier > 799){
			hero.x -= hero.speed * modifier;	
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			
		}
		if (hero.y < 54 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 522 && hero.y > 54 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if (hero.y > 522 && hero.y < 590 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x + hero.speed * modifier < 1003 ){
			hero.x += hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 54 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
	}
//*************************************************************************************************
	if (hero.y > 785){
		zimmer = "flur1" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (hero.y < 20){
		hero.x = -50;
		hero.y= -50;
		zimmer="end"
	}
	if (13 in keysDown && hero.x < 810 && hero.y > 180 && hero.y < 394) {
				zimmer="treppe1aufend"

	}
}
//*************************************************************************************************
//                                     UPDATE - treppe1auf                                        *
//*************************************************************************************************
else if(zimmer == "treppe1auf"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 799 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && hero.x < 799 && hero.x > 731 && hero.y - hero.speed * modifier > 220 ){
			hero.y -= hero.speed * modifier;
		}			
		if(hero.y < 590 && hero.x < 731 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 788 ){
			hero.y += hero.speed * modifier;
		}
		if( (hero.x< 862 && hero.x > 799) || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 748){
				hero.y += hero.speed * modifier;
			}			
		}
		if (hero. x < 799 && hero.x > 731 && hero.y + hero.speed * modifier < 352 ){
				hero.y += hero.speed * modifier;
		}
		if (hero.x < 731 && hero.y + hero.speed * modifier < 362){
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	// key left
		document.getElementById("move").play()	
		if(hero.y < 522 ){
			if ((hero.y > 221 && hero.y < 353) || hero.x < 790){
				if(hero.x - hero.speed * modifier > 572){ 
					hero.x -= hero.speed * modifier;
				}
			}
			else if ( hero.x - hero.speed * modifier > 799 ){
				hero.x -= hero.speed * modifier;
			}
				
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 522 && hero.x > 799 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if(hero.y < 522 && hero.x < 799 ){
			if ( hero.y < 221 || hero.y > 353 ){
				if (hero.x+ hero.speed * modifier < 731){
					hero.x += hero.speed * modifier;
				}
			}
			else if(hero.x+ hero.speed * modifier < 1003){
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y > 522 && hero.y < 590 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x + hero.speed * modifier < 1003 ){
			hero.x += hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
	}
//*************************************************************************************************
	if (hero.y > 785){
		zimmer = "flur1" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (32 in keysDown && hero.x < 731 ) {
		if (allfound){
				zimmer="treppe1selend"
			}
			else{
				zimmer="treppe1sel"
			}
		pointer.x =1220
		pointer.y=400  
	}
	if ( nosefound==false && hero.x > 619 && hero.x < 690 && hero. y > 259 && hero.y < 328){
		ctx.fillStyle="green"
		ctx.fillRect(15,888,17,17);
		if (nosefound==false){document.getElementById("win").play()}
			nosefound=true;
			if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
		console.log("nase")
	}
}
//*************************************************************************************************
//                                     UPDATE - treppe1aufend                                        *
//*************************************************************************************************
else if(zimmer == "treppe1aufend"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 14 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && (hero.x < 862 || hero.x > 995) && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && hero.x < 799 && hero.x > 731 && hero.y - hero.speed * modifier > 220 ){
			hero.y -= hero.speed * modifier;
		}			
		if(hero.y < 590 && hero.x < 731 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 788 ){
			hero.y += hero.speed * modifier;
		}
		if( (hero.x< 862 && hero.x > 799) || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 748){
				hero.y += hero.speed * modifier;
			}			
		}
		if (hero. x < 799 && hero.x > 731 && hero.y + hero.speed * modifier < 352 ){
				hero.y += hero.speed * modifier;
		}
		if (hero.x < 731 && hero.y + hero.speed * modifier < 362){
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	// key left
		document.getElementById("move").play()	
		if(hero.y < 522 && hero.y > 54 ){
			if ((hero.y > 221 && hero.y < 353) || hero.x < 790){
				if(hero.x - hero.speed * modifier > 572){ 
					hero.x -= hero.speed * modifier;
				}
			}
			else if ( hero.x - hero.speed * modifier > 799 ){
				hero.x -= hero.speed * modifier;
			}
				
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y < 54 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 522 && hero.y > 54 && hero.x > 799 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if(hero.y < 522 && hero.y > 54 && hero.x < 799 ){
			if ( hero.y < 221 || hero.y > 353 ){
				if (hero.x+ hero.speed * modifier < 731){
					hero.x += hero.speed * modifier;
				}
			}
			else if(hero.x+ hero.speed * modifier < 1003){
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y > 522 && hero.y < 590 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x + hero.speed * modifier < 1003 ){
			hero.x += hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
		if (hero.y < 54 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
	}
//*************************************************************************************************
	if (hero.y > 785){
		zimmer = "flur1" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (hero.y < 20){
		hero.x = -50;
		hero.y= -50;
		zimmer="end"
	}
	if (32 in keysDown && hero.x < 731 ) {
		if (allfound){
				zimmer="treppe1selend"
			}
			else{
				zimmer="treppe1sel"
			}
		pointer.x =1220
		pointer.y=400  
	}
	
}
//*************************************************************************************************
//                                     UPDATE - treppe2auf                                        *
//*************************************************************************************************
else if(zimmer == "treppe2auf"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 799 && hero.y - hero.speed * modifier > 54 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && hero.x < 799 && hero.x > 731 && hero.y - hero.speed * modifier > 220 ){
			hero.y -= hero.speed * modifier;
		}			
		if(hero.y < 590 && hero.x < 731 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 788 ){
			hero.y += hero.speed * modifier;
		}
		if( (hero.x< 862 && hero.x > 799) || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 748){
				hero.y += hero.speed * modifier;
			}			
		}
		if (hero. x < 799 && hero.x > 731 && hero.y + hero.speed * modifier < 352 ){
				hero.y += hero.speed * modifier;
		}
		if (hero.x < 731 && hero.y + hero.speed * modifier < 362){
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	// key left
		document.getElementById("move").play()	
		if(hero.y < 522 ){
			if ((hero.y > 221 && hero.y < 353) || hero.x < 790){
				if(hero.x - hero.speed * modifier > 572){ 
					hero.x -= hero.speed * modifier;
				}
			}
			else if ( hero.x - hero.speed * modifier > 799 ){
				hero.x -= hero.speed * modifier;
			}
				
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 522 && hero.x > 799 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if(hero.y < 522 && hero.x < 799 ){
			if ( hero.y < 221 || hero.y > 353 ){
				if (hero.x+ hero.speed * modifier < 731){
					hero.x += hero.speed * modifier;
				}
			}
			else if(hero.x+ hero.speed * modifier < 1003){
				hero.x += hero.speed * modifier;
			}
		}
		if (hero.y > 522 && hero.y < 590 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x + hero.speed * modifier < 1003 ){
			hero.x += hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x + hero.speed * modifier < 994){
			hero.x += hero.speed * modifier;
		}
	}
//*************************************************************************************************
	if (hero.y > 785){
		zimmer = "flur2" 
		hero.y = 54;
		hero.x = 550;
		document.getElementById("move").pause()
	}
	if (32 in keysDown && hero.x < 731 ) {
		zimmer="treppe2sel"
		pointer.x =1220
		pointer.y=400  
	}
}
//*************************************************************************************************
//                                     UPDATE - treppe0auf                                        *
//*************************************************************************************************
else if(zimmer == "treppe0auf"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.x < 1003 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 799 && hero.x < 1003 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && hero.x < 799 && hero.x > 731 && hero.y - hero.speed * modifier > 220 ){
			hero.y -= hero.speed * modifier;
		}			
		if(hero.y < 590 && hero.x < 731 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 1003 && hero.y - hero.speed * modifier > 460 ){
			hero.y -= hero.speed * modifier;
		}
		
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 522 ){
			hero.y += hero.speed * modifier;
		}
		if( (hero.x< 862 && hero.x > 799) || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}			
		}
		if (hero. x < 799 && hero.x > 731 && hero.y + hero.speed * modifier < 352 ){
				hero.y += hero.speed * modifier;
		}
		if (hero.x < 731 && hero.y + hero.speed * modifier < 362){
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	// key left
		document.getElementById("move").play()	
		if(hero.y < 522 ){
			if ((hero.y > 221 && hero.y < 353) || hero.x < 790){
				if(hero.x - hero.speed * modifier > 572){ 
					hero.x -= hero.speed * modifier;
				}
			}
			else if ( hero.x - hero.speed * modifier > 799 ){
				hero.x -= hero.speed * modifier;
			}
				
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 460 && hero.x > 799 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if( hero.y > 460){
			if( hero.x+ hero.speed * modifier < 1043){
				hero.x += hero.speed * modifier;
			}	
		}
		if(hero.y < 522 && hero.x < 799 ){
			if ( hero.y < 221 || hero.y > 353 ){
				if (hero.x+ hero.speed * modifier < 731){
					hero.x += hero.speed * modifier;
				}
			} 
			else if(hero.x+ hero.speed * modifier < 1003){
				hero.x += hero.speed * modifier;
			}
		}
	}
//*************************************************************************************************
	if (hero.x > 1040){
		zimmer = "garage" 
		hero.y = -50;
		hero.x = -50;
		document.getElementById("move").pause()
		pointer.x = 500;
		pointer.y = 200
	}
	if (32 in keysDown && hero.x < 731 ) {
		zimmer="treppe0sel"
		pointer.x =1220
		pointer.y=400  
	}
}
//*************************************************************************************************
//                                     UPDATE - treppe0zu                                        *
//*************************************************************************************************
else if(zimmer == "treppe0zu"){																			
	if (38 in keysDown) { 	
		document.getElementById("move").play()	// key up
		if(hero.y > 590 && hero.x < 862  && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 &&  hero.x > 995 && hero.x < 1003 && hero.y - hero.speed * modifier > 590 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y > 590 && hero.x > 862 && hero.x < 995 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
		if(hero.y < 590 && hero.x > 799 && hero.x < 1003 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}				
		if(hero.y < 590 && hero.x < 799 && hero.x > 731 && hero.y - hero.speed * modifier > 220 ){
			hero.y -= hero.speed * modifier;
		}			
		if(hero.y < 590 && hero.x < 731 && hero.y - hero.speed * modifier > 212 ){
			hero.y -= hero.speed * modifier;
		}
		if (hero.x > 1003 && hero.y - hero.speed * modifier > 460 ){
			hero.y -= hero.speed * modifier;
		}
		
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		document.getElementById("move").play()	
		if( hero.x > 862 && hero.x < 995 && hero.y + hero.speed * modifier < 522 ){
			hero.y += hero.speed * modifier;
		}
		if( (hero.x< 862 && hero.x > 799) || hero.x > 995 ){
			if (hero.y < 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}
			if (hero.y > 522 && hero.y + hero.speed * modifier < 522){
				hero.y += hero.speed * modifier;
			}			
		}
		if (hero. x < 799 && hero.x > 731 && hero.y + hero.speed * modifier < 352 ){
				hero.y += hero.speed * modifier;
		}
		if (hero.x < 731 && hero.y + hero.speed * modifier < 362){
			hero.y += hero.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 	// key left
		document.getElementById("move").play()	
		if(hero.y < 522 ){
//			if ((hero.y > 221 && hero.y < 353) || hero.x < 790){
//				if(hero.x - hero.speed * modifier > 572){ 
//					hero.x -= hero.speed * modifier;
//				}
//			}
			//else 
				if ( hero.x - hero.speed * modifier > 799 ){
				hero.x -= hero.speed * modifier;
			}
				
		}
		if (hero.y > 522 && hero.y < 590 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;			
		}
		if (hero.y > 590 && hero.y < 748 && hero.x - hero.speed * modifier > 853 ){
			hero.x -= hero.speed * modifier;
		}
		if (hero.y > 748 && hero.x - hero.speed * modifier > 862){
			hero.x -= hero.speed * modifier;
		}
	}	
//*************************************************************************************************
	if (39 in keysDown) { 	
		document.getElementById("move").play()	// key right
		if(hero.y < 460 && hero.x > 799 && hero.x+ hero.speed * modifier < 1003){
			hero.x += hero.speed * modifier;	
		}
		if( hero.y > 460){
			if( hero.x+ hero.speed * modifier < 1043){
				hero.x += hero.speed * modifier;
			}	
		}
		if(hero.y < 522 && hero.x < 799 ){
			if ( hero.y < 221 || hero.y > 353 ){
				if (hero.x+ hero.speed * modifier < 731){
					hero.x += hero.speed * modifier;
				}
			} 
			else if(hero.x+ hero.speed * modifier < 1003){
				hero.x += hero.speed * modifier;
			}
		}
	}
//*************************************************************************************************
	if (hero.x > 1040){
		zimmer = "garage" 
		hero.y = -50;
		hero.x = -50;
		document.getElementById("move").pause()
		pointer.x = 500;
		pointer.y = 200
	}
	if (13 in keysDown && hero.x < 810 && hero.y > 180 && hero.y < 394) {
		zimmer="treppe0auf"  
	}
}
//*************************************************************************************************
//                           UPDATE - TREPPE1SEL bis 3sel und end                                 *
//*************************************************************************************************
else if(zimmer == "treppe1sel" || zimmer=="treppe2sel" || zimmer=="treppe0sel" || zimmer=="treppe1selend"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 229){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 588){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 1194){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1362){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (13 in keysDown && pointer.x < 1342 && pointer.x > 1247) {
		if (pointer.y < 324 && pointer.y > 229){
			zimmer="treppe2auf"
			pointer.x=-10
			pointer.y=-10
		}
		if (pointer.y < 451 && pointer.y > 356){
			if (allfound){
				zimmer="treppe1aufend"
			}
			else{
				zimmer="treppe1auf"
			}
			pointer.x=-10
			pointer.y=-10
		}
		if (pointer.y < 1241 && pointer.y > 486){
			zimmer="treppe0auf"
			pointer.x=-10
			pointer.y=-10
		}
	}
}
//*************************************************************************************************
//                                       UPDATE - BIB                                             *
//*************************************************************************************************
else if(zimmer == "bib"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1494){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (pointer.x > 674 && pointer.x < 694 && pointer.y > 566 && pointer.y < 581){
		ctx.fillStyle="green"
		ctx.fillRect(15,868,17,17);
		if (earlfound==false){document.getElementById("win").play()}
		earlfound=true;
					if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
	}
	if (32 in keysDown) {
		zimmer="flur1"
		hero.x = 55
		hero.y =380
		pointer.x =-10
		pointer.y=-10  
	}
}
//*************************************************************************************************
//                                       UPDATE - slz                                            *
//*************************************************************************************************
else if(zimmer == "slz"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1494){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (pointer.x > 1164 && pointer.x < 1190 && pointer.y > 344 && pointer.y < 370){
		ctx.fillStyle="green"
		ctx.fillRect(150,848,17,17);
		if (eyefound==false){document.getElementById("win").play()}
		eyefound=true;
					if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
	}
	if (32 in keysDown) {
		zimmer="flur2"
		hero.x = 620
		hero.y = 561
		pointer.x =-10
		pointer.y=-10  
	}
}
//*************************************************************************************************
//                                       UPDATE - pool                                            *
//*************************************************************************************************
else if(zimmer == "pool"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1494){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (pointer.x > 166 && pointer.x < 213 && pointer.y > 646 && pointer.y < 694){
		ctx.fillStyle="green"
		ctx.fillRect(15,928,17,17);
		if (teethfound==false){document.getElementById("win").play()}
		teethfound=true;
					if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
	}
	if (32 in keysDown) {
		zimmer="flur2"
		hero.x = 55
		hero.y =380
		pointer.x =-10
		pointer.y=-10  
	}
}
//*************************************************************************************************
//                                     UPDATE - GARAGE                                            *
//*************************************************************************************************
else if(zimmer == "garage"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1494){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (pointer.x > 96 && pointer.x < 137 && pointer.y > 344 && pointer.y < 364){
		ctx.fillStyle="green"
		ctx.fillRect(150,868,17,17);
		if (earrfound==false){document.getElementById("win").play()}
		earrfound=true;
					if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
	}
	if (32 in keysDown) {
		zimmer="treppe0zu"
		hero.x = 1003
		hero.y =490
		pointer.x =-10
		pointer.y=-10  
	}
}
//*************************************************************************************************
//                                       UPDATE - CAFE-R                                          *
//*************************************************************************************************
else if(zimmer == "cafer"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1494){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (32 in keysDown) {
		zimmer="flur1"
		hero.x = 620
		hero.y = 561
		pointer.x =-10
		pointer.y=-10  
	}
	if(pointer.x < 4){
		zimmer="cafel"
		pointer.x=1493
	}
}
//*************************************************************************************************
//                                       UPDATE - CAFE-L                                          *
//*************************************************************************************************
else if(zimmer == "cafel"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1495){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (32 in keysDown) {
		zimmer="flur1"
		hero.x = 620
		hero.y = 561
		pointer.x =-10
		pointer.y=-10  
	}
	if(pointer.x > 1493){
		zimmer="cafer"
		pointer.x=5
	}
	if (13 in keysDown) {
		if (pointer.x > 509 && pointer.x < 778 && pointer.y > 325 && pointer.y < 462){ // rechts auf
			zimmer="micror"
			pointer.x= 810
			pointer.y= 500
		}
		if (pointer.x > 179 && pointer.x < 431 && pointer.y > 323 && pointer.y < 451){ // links auf
			zimmer="microl"
			pointer.x= 460
			pointer.y= 490
		}
	}
}
//*************************************************************************************************
//                                    UPDATE - MICROWELLE-R                                       *
//*************************************************************************************************
else if(zimmer == "micror"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1495){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (32 in keysDown) {
		zimmer="flur1"
		hero.x = 620
		hero.y = 561
		pointer.x =-10
		pointer.y=-10  
	}
	if(pointer.x > 1493){
		zimmer="cafer"
		pointer.x=5
	}
	if (13 in keysDown) {
		if (pointer.x > 552 && pointer.x < 805 && pointer.y > 359 && pointer.y < 492){
			zimmer="cafel"
			pointer.x= 780
			pointer.y= 466

		}
		if (pointer.x > 197 && pointer.x <416 && pointer.y >349 && pointer.y < 481){
			zimmer="microl"
				pointer.x= 460
			    pointer.y= 490
		}
	}
	if (pointer.x > 666 && pointer.x < 706 && pointer.y > 436 && pointer.y < 457){
		ctx.fillStyle="green"
		ctx.fillRect(15,908,17,17);
		if (lipsfound==false){document.getElementById("win").play()}
		lipsfound=true;
					if ( earlfound==true && lipsfound==true && nosefound==true && earrfound==true && teethfound==true && eyefound==true){
				allfound=true;
			}
	}
}
//*************************************************************************************************
//                                    UPDATE - MICROWELLE-L                                      *
//*************************************************************************************************
else if(zimmer == "microl"){																			
	if (38 in keysDown) { 																			// key up
		if ( pointer.y - pointer.speed * modifier > 0){
			pointer.y -=pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (40 in keysDown) { 																			// key down
		if( pointer.y +pointer.speed * modifier < 838){ 										
			pointer.y +=pointer.speed * modifier;
		}
	}	
//*************************************************************************************************	
	if (37 in keysDown) { 																			// key left
		if(pointer.x - pointer.speed * modifier > 0){ 						
			pointer.x -= pointer.speed * modifier;
		}	
	}	
//*************************************************************************************************
	if (39 in keysDown) { 																			// key right	
		if ( pointer.x + pointer.speed * modifier < 1495){ 
			pointer.x +=pointer.speed * modifier;
		}
	}
//*************************************************************************************************
	if (32 in keysDown) {
		zimmer="flur1"
		hero.x = 620
		hero.y = 561
		pointer.x =-10
		pointer.y=-10  
	}
	if(pointer.x > 1493){
		zimmer="cafer"
		pointer.x=5
	}
	if (13 in keysDown) {
		if (pointer.x > 212 && pointer.x < 451 && pointer.y > 363 && pointer.y < 487){
			zimmer="cafel"
			pointer.x = 420
			pointer.y=485
		}
		if (pointer.x > 529 && pointer.x <790 && pointer.y >375 && pointer.y < 500){
			zimmer="micror"
			pointer.x= 810
			pointer.y= 500
		}
	}
}
}	

function render(){	
 							// zeichnet einzelne images ins canvas	
	console.log("zimmer: "+zimmer)
    if (terasseready && zimmer == "terasse" ) {        
		ctx.drawImage(terasseimage, 0,0);
	}
	if (flur1ready && zimmer == "flur1" ) {        
		ctx.drawImage(flur1image, 0,0);
	}

	if (flur2ready && zimmer == "flur2" ) {        
		ctx.drawImage(flur2image, 0,0);
	}
	if (caferready && zimmer == "cafer" ) {        
		ctx.drawImage(caferimage, 0,0);
	}
	if (cafelready && zimmer == "cafel" ) {        
		ctx.drawImage(cafelimage, 0,0);
	}
	if (bibready && earlready && zimmer == "bib"){
		ctx.drawImage(bibimage, 0,0);
				if (earlfound==false){
					ctx.drawImage(earlimage, 680,570);
				}
	}
	if (microrready && lipsready && zimmer == "micror"){
		ctx.drawImage(microrimage, 0,0);
				if (lipsfound==false){
					ctx.drawImage(lipsimage, 670,440);
				}
	}
	if (microlready && zimmer == "microl"){
		ctx.drawImage(microlimage, 0,0);
	}
	if (treppe1zuready && zimmer == "treppe1zu" ) {        
		ctx.drawImage(treppe1zuimage, 0,0);
	}
	
	if (treppe1zuendready && zimmer == "treppe1zuend" ) {        
		ctx.drawImage(treppe1zuendimage, 0,0);
	}
	if (treppe1aufendready && zimmer == "treppe1aufend" ) {        
		ctx.drawImage(treppe1aufendimage, 0,0);
	}
	if (treppe1selendready && zimmer == "treppe1selend" ) {        
		ctx.drawImage(treppe1selendimage, 0,0);
	}
	
	if (treppe0zuready && zimmer == "treppe0zu" ) {        
		ctx.drawImage(treppe0zuimage, 0,0);
	}
	if (endready && zimmer == "end" ) {        
		ctx.drawImage(endimage, 0,0);
	}
	if (treppe2zuready && zimmer == "treppe2zu" ) {        
		ctx.drawImage(treppe2zuimage, 0,0);
	}
	if (slzready && zimmer == "slz" ) {        
		ctx.drawImage(slzimage, 0,0);
		if(eyefound==false){
			ctx.drawImage(eyeimage, 1170,350);	
		}
	}
	if (poolready && zimmer == "pool" ) {        
		ctx.drawImage(poolimage, 0,0);
		if(teethfound==false){
			ctx.drawImage(teethimage, 170,650);	
		}
	}
	if (treppe2aufready && zimmer == "treppe2auf" ) {        
		ctx.drawImage(treppe2aufimage, 0,0);
	}
	if (treppe0aufready && zimmer == "treppe0auf" ) {        
		ctx.drawImage(treppe0aufimage, 0,0);
	}
	if (treppe1aufready && zimmer == "treppe1auf" ) {        
		ctx.drawImage(treppe1aufimage, 0,0);
		if(nosefound==false){
			ctx.drawImage(naseimage, 660,300);
		}
	}
	if (garageready && zimmer == "garage" ) {        
		ctx.drawImage(garageimage, 0,0);
		if(earrfound==false){
			ctx.drawImage(earrimage, 100,350);
		}
	}
	if (treppe1selready && zimmer == "treppe1sel" ) {        
		ctx.drawImage(treppe1selimage, 0,0);
	}
		if (treppe0selready && zimmer == "treppe0sel" ) {        
		ctx.drawImage(treppe0selimage, 0,0);
	}
		if (treppe2selready && zimmer == "treppe2sel" ) {        
		ctx.drawImage(treppe2selimage, 0,0);
	}
	if (heroready) {
        ctx.drawImage(heroimage, hero.x,hero.y);
	}
	if(pointerready){
		ctx.drawImage(pointerimage, pointer.x,pointer.y);
	}

};	
//******************************************************************************************************

var main = function () {							// sorgt für immer wieder erneuerung des canvas
	var now = Date.now();
	var delta = now - then;
	update(delta / 1000);
	then = now;
	requestAnimationFrame(main);
}
var w = window;
requestAnimationFrame = w.requestAnimationFrame       ||
                        w.webkitRequestAnimationFrame || 
						w.msRequestAnimationFrame     || 
						w.mozRequestAnimationFrame;
var then = Date.now();
window.onload = function(){ main();}

