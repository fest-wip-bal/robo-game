#FeSt RoboGame ReadMe  
  


##Spielerklärung

Das **FeSt RoboGame** ist ein Spiel, mit welchem der Nutzer die FHDW in Mettmann besser kennen lernen kann.
Durch Steuerung des Robotters Rob Otter mit den Pfeiltasten kann der Spieler Rob helfen seine verschwundenen Sinnesorgane zu finden.    

Räume werden durch Überfahren der farblich dunnkler dargestellten Flächen betreten und können durch Drücken der Leertaste wieder verlassen werden.

In den einzelnen Räumen selber ist ein Laserpointer durch die Pfeiltasten zu steuern. Trifft der Punkt  auf ein verstecktes Körperteil, so wird es als gefunden  markiert.

Ziel ist es, alle unter dem Bild aufgelisteten Sinnesorgane in der FHDDW zu finden, damit Rob Otter wieder komplett ist.

[zum RoboGame-Download](https://bitbucket.org/fest-wip-bal/robo-game)   
[mehr Infos](https://festrobogame.wordpress.com/)


##Hintergrund
Das FeSt Robo-Game soll dabei das Interesse verschiedener Zielgruppen an der FHDW als besondere Hochschule steigern.

Das Robo-Game soll durch die grafische Anlehnung an Pokemon-Spiele auf dem Gameboy nostalgische Gedanken auslösen und gleichzeitig die FHDW von seiner Schokoladenseite präsentieren.

Die Verbindung aus bewegten Bildern, die Aufmerksamkeit wecken und von früher bekannten Bildmustern soll Jugendliche dazu anhalten den Messestand der FHDW besonders zu betrachten.
Ein realer Eindruck der Hochschule soll weitergehend durch die Präsenz von Studierenden und Fotos der Hochschule erzeugt werden.

Ziel der Kombination aus einem Roboter und wenig konkreten Darstellungen ist das Design eines modernen Images einer Hochschule, die seinen Studierenden Platz für künstlerische Freiheiten  und Visionen bietet.


## FAQs
**Woran erkenne ich, dass ich einen Raum betreten kann?**  
Raumeingänge sind durch "Bodenmarkierungen in der Wand" gekennzeichnet. Du erkennst sie an der dunkleren Farbgebung.

**Wie kann ich einen Raum betreten?**  
Du kannst einen Raum betreten, indem du den Roboter auf die Rechtecke lenkst, die dunkler dargestellt sind, als der übrige Fußboden.

**Wie kann ich einen Raum mit Bildhintergrund wieder verlassen?**  
Du kannst einen Raum verlassen, indem du die Leertaste benutzt.

**Wie finde ich die versteckten Sinnesorgane?**   
//Achte auf die Hinnweise, die Rob dir im Spiel gibt.// erst ab der Folgevesion

**Wie öffne ich die Mikrowellen in der Cafete?**  
Du kannst die Mikrowellen öffnen, indem du den Laser Point auf die ausgewählte Mikrowelle lenkst und die Eingabetaste drückst.

**Wie verwende ich den Aufzug?**  
Du kannst den Aufzug betreten, indem du Rob vor die Aufzugtür stellst und zum Ruf des Aufzugs die Enter- oder Eingabetaste drückst. Dann kannst du Rob in den Aufzug steigen lassen und die Leertaste drücken.  
Es erscheint ein Auswahlfeld, in dem du mit dem roten Laser-Point auswählen kannst auf welche der Ebenen du möchtest. Hast du den rote Punkt auf de entsprechende Zahl gelenkt, bestätigst du diese Eingabe mit der Eingabetaste.   

**Warum kann ich das Treppenhaus im Spiel nicht benutzen?**  
Das Treppenhaus ist für dich als Spieler des Robotters nicht möglich, da dieser in unserer vorstellung Rollen hat und keine Treppenstufen laufen kann.


## Planung für zukünftige Spielfunktionen
Da es sich bei der Version noch nicht um die endgültige Version des Robo-Games handelt, sind noch weitere Spielfunktionen geplant. Dazu gehören weitere eingebundene Minispiele, wie etwa das fangen der Nase im Bonbonregen des Aufzugs, so wie ausführlich dargestelte Interaktion zwischen Rob Otter und anderen "zum Leben erwachten" Gegenständen der FHDW.

Außerdem soll Rob mit dem Spieler kommunizieren, so dass dieser den Charme und Humor des liebenswürdigen Roboters erleben kann.
Auch Steuerungstexte sind geplant.

Zusätzlich sollen zukünftig weitere Informationen über das Studienangebot der FHDW geliefert werden.


##Hilfen bei der Abreit
Im Rahmen der Programmierung wurden folgende Bibliotheken und Codeteile genutzt:

(http://www.lostdecadegames.com/how-to-make-a-simple-html5-canvas-game)                        
als Grundlage für die Bewegungssteuerung des Robotters.

(https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)  
als Möglichkeit für die regelmäßige Erneuerung des bewegten Bildes. 






