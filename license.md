#####Unser Programm unterliegt der **MIT-Lizeenz**.

###The MIT License (MIT)


Copyright &copy; 2015  Dominik Felske und Judith Storb


Permission is hereby granted, free of charge, to any person obtaining a copy
of this Robo-Game software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Die im Spiel verwendeten **Bilder und Videos** sind von uns selber erstellt und entsprechen den Tatsachen im September 2015.
Verwendete **Grafiken** sind ebenfalls nach Vorlage der realen Hochschule selber erstellt.
Verwendete **Audiodateien** sind von der Seite (www.soundbible.com) und stehen unter der Creative Common Lizenz. 
Verwendet wurden die Sounds [TA-DA](http://soundbible.com/1003-Ta-Da.html) und [SMALL ELECTRIC MOTOR](http://soundbible.com/1427-Small-Electrical-Motor.html)